#!/bin/bash

if [ "$#" -ge 1 ];
then
	EnvironmentName=$1
else
	EnvironmentName="default"
fi


# set up environmental paths
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done


DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"


if [ "$EnvironmentName" = "default" ]; #Default is python3
then
	export PATH=/afs/cern.ch/work/c/chlcheng/public/local/conda/miniconda/envs/ml-base/bin:$PATH
elif [ "$EnvironmentName" = "dev" ]; #Default is python3
then
	export PATH=/afs/cern.ch/work/c/chlcheng/public/local/conda/miniconda/envs/ml-base/bin:$PATH	
	export PATH=${DIR}/bin:${PATH}
	export PYTHONPATH=${DIR}:${PYTHONPATH}	
elif [[ "$EnvironmentName" = "conda" ]];
then	
	__conda_setup="$('/afs/cern.ch/work/c/chlcheng/public/local/conda/miniconda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
	if [ $? -eq 0 ]; then
	    eval "$__conda_setup"
	else
	    if [ -f "/afs/cern.ch/work/c/chlcheng/public/local/conda/miniconda/etc/profile.d/conda.sh" ]; then
	        . "/afs/cern.ch/work/c/chlcheng/public/local/conda/miniconda/etc/profile.d/conda.sh"
	    else
	        export PATH="/afs/cern.ch/work/c/chlcheng/public/local/conda/miniconda/bin:$PATH"
	    fi
	fi
	unset __conda_setup

	conda activate ml-base		
elif [[ "$EnvironmentName" = "python2" ]];
then
	__conda_setup="$('/afs/cern.ch/work/c/chlcheng/public/local/conda/miniconda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
	if [ $? -eq 0 ]; then
	    eval "$__conda_setup"
	else
	    if [ -f "/afs/cern.ch/work/c/chlcheng/public/local/conda/miniconda/etc/profile.d/conda.sh" ]; then
	        . "/afs/cern.ch/work/c/chlcheng/public/local/conda/miniconda/etc/profile.d/conda.sh"
	    else
	        export PATH="/afs/cern.ch/work/c/chlcheng/public/local/conda/miniconda/bin:$PATH"
	    fi
	fi
	unset __conda_setup

	conda activate ml-base-py2	
fi